class CreatePositionHistory < ActiveRecord::Migration[6.1]
  def change
    create_table :position_histories do |t|
      t.string :name, null: false
      t.belongs_to :position, null: false
      t.datetime :begin_date, null: false
      t.datetime :end_date
      t.timestamps
    end
  end
end
