FactoryBot.define do
  factory :division, class: "Division" do
    name { Faker::Lorem.word }
  end
end