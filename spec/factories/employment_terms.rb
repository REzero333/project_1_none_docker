FactoryBot.define do
  factory :employment_term, class: "EmploymentTerm" do
    begin_date { DateTime.now }
  end
end