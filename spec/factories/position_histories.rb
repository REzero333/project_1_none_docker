FactoryBot.define do
  factory :position_history, class: "PositionHistory" do
    name { Faker::Lorem.word }
    begin_date { DateTime.now }
  end
end