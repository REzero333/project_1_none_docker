require "rails_helper"

RSpec.describe "TakeUserDataOnFront", type: :class do
before do
  User.delete_all
  EmploymentTerm.delete_all
  Position.delete_all
  Division.delete_all
  PositionHistory.delete_all
end

  it 'Выведет всех User и их должности' do
    5.times{ create_employment_term }
    result = service.call.to_s
    p result
    User.find_each do |user|
      full_name = "#{user.last_name} #{user.first_name} #{user.surname}"
      expect(result.include?(full_name)).to be_truthy
    end
  end

  context 'Выведет историю пользователя' do 
    before do 
      @e_t = create_employment_term
      @first_date = DateTime.now - 1.days
      @last_date = DateTime.now + 1.days
    end
    it 'Есть записи' do
      result = service.history(@first_date, @last_date, @e_t.user_id)
      expect(result.include?(@e_t.position.name)).to be_truthy
    end

    it 'Нет записей' do 
      @first_date = @first_date - 10.days
      @last_date = @last_date - 10.days
      result = service.history(@first_date, @last_date, @e_t.user_id)
      expect(result).to eq("Нет записей")
    end
  end

  def service
    TakeUserDataOnFront
  end

  def create_employment_term
    user = FactoryBot.create(:user)
    position = FactoryBot.create(:position)
    position_h = FactoryBot.create(:position_history, position_id: position.id)
    division = FactoryBot.create(:division)
    employment_term = FactoryBot.create(:employment_term,
                                          position_id: position.id,
                                          division_id: division.id,
                                          user_id: user.id)
    employment_term
  end
end