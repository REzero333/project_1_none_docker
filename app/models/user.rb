class User < ActiveRecord::Base
	before_destroy :destroy_employment_term

	def destroy_employment_term
		e_t = EmploymentTerm.find_by_user_id(self.id)
		return if e_t.nil?
		e_t.position.history.close
		e_t.delete
	end
end