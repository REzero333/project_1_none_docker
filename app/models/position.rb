class Position < ActiveRecord::Base
	
	def name
		PositionHistory.find_by_position_id(self.id).name
	end

	def create_name=(name)
		history = PositionHistory.new
		history.name = name
		self.save! if new_record?
		history.position_id = self.id
		history.begin_date = DateTime.now
		history.save!
	end

	def update_name=(name)
		PositionHistory.where(position_id: self.id).update_all(name: name)
	end

	def history(begin_date = nil)
		if begin_date.present?
			PositionHistory.where(position_id: self.id, end_date: nil)
												 .where("`position_histories`.`begin_date` >= ? AND `position_histories`.`begin_date` <= ?", begin_date.beginning_of_day , begin_date.end_of_day).take
		else
			PositionHistory.where(position_id: self.id, end_date: nil).take
		end
	end
end