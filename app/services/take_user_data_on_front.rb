class TakeUserDataOnFront

	def initialize(first_date: nil, last_date: nil, user_id: nil)
		@first_date = first_date
		@last_date = last_date
		@user_id = user_id
	end

	def self.call
		serivce = TakeUserDataOnFront.new
		serivce.fill_data
	end

	def self.history(first_date, last_date, user_id)
		serivce = TakeUserDataOnFront.new(first_date: first_date, last_date: last_date, user_id: user_id)
		serivce.take_history
	end

	def take_history
		employment_terms = EmploymentTerm.where(user_id: @user_id.to_i)
																		 .where("`employment_terms`.`begin_date` >= ? AND `employment_terms`.`begin_date` <= ?", @first_date.to_datetime.beginning_of_day , @last_date.to_datetime.end_of_day)
		return "Нет записей" if employment_terms.blank?
		@html = "<table class='table'>"
		employment_terms.each do |e_t|
			end_date = e_t.end_date ? e_t.end_date.to_date.to_s : "Работает"
			@html += "<tr><td>#{e_t.position.name}</td><td>/</td><td>#{e_t.begin_date.to_date.to_s}</td><td>-</td><td>#{end_date}</td></tr>"
		end
		@html += "<table>"
		@html
	end

	def fill_data
		@filled_data = {}
		fill_users_and_positions
		build_html
	end

	def build_html
		@html = ''
		sort_data = @filled_data.keys.sort
		data = []
		sort_data.map{ |user_name| data << [user_name, @filled_data[user_name]]}
		@html += "<table class='table'>"
		data.each do |d|
			user_name = d.first.split(":").first
			id = d.first.split(":").last
			@html += "<tr><td>#{user_name}</td><td>/</td><td>#{d.last}</td><td><button onclick='show_history(#{id})' class='btn'>История</button></td></tr>"
		end
		@html += "</table>"
		@html
	end

	def fill_users_and_positions
		users.find_each do |user|
			e_t = employment_terms.detect{ |e_t| e_t.user_id == user.id }
			pos = e_t.present? ? position.detect{ |pos| pos.id == e_t.position_id }.name : "Не работает"
			@filled_data["#{user.last_name} #{user.first_name} #{user.surname}:#{user.id}"] = pos
		end
	end

	def users
		@users ||= User.where(id: employment_terms.pluck(:user_id))
	end

	def employment_terms
		@employment_terms ||= EmploymentTerm.where(end_date: nil)
																				.select(:user_id, :position_id, :division_id)
	end

	def position
		@position ||= Position.select("*")
	end

	def divisions
		@division ||= Division.select("*")
	end
end