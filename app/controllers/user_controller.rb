class UserController < ApplicationController

	def create
		user = User.new
		user.first_name = user_params[:first_name]
		user.last_name =  user_params[:last_name]
		user.surname =  user_params[:surname]
		user.save!
		redirect_to '/'
	end

	def update
		user = User.find(user_params[:id])
		return redirect_to '/' if user.nil?
		user.first_name = user_params[:first_name] if user_params[:first_name].present?
		user.last_name = user_params[:last_name] if user_params[:last_name].present?
		user.surname = user_params[:surname] if user_params[:surname].present?
		user.save!
		redirect_to '/' 
	end

	def delete
		User.find(user_params[:id]).try(:destroy)
		redirect_to '/'
	end

	def user_params
		@user_params ||= params.permit(:first_name, :last_name, :surname, :id)
	end
end